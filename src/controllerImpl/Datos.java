/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllerImpl;

/**
 *
 * @author ANA_MARIA
 */
public interface Datos {
    public String mostrarDatos();
    public String mostrarNombres(); 
    public double mostrarSalarioNeto();
    public double mostrarSalarioBase();
    public double mostrarEPS();
    public double mostrarPension();
    public double mostrarARL();
    public double mostrarSena();
    public double mostrarCajas();
    public double mostrarICBF();
    public double mostrarAuxilio();
    public double mostrarLiquidacion();
    public String imprimirEncabezadoLista();

}
