/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;
import controller.*;

/**
 *
 * @author ANA_MARIA
 */
public class Vista {

    private static int opcionDC = 0;
    private static int opcionOC = 0;
    private static int opcionHC = 0;
    private static int opcionAUX = 0;
    private static int opcionTEC = 0;
    private static int opcionPROF = 0;
    private static int opcionOPS = 0;

    public Vista() {
    }

    public static int getOpcionOC() {
        return opcionOC;
    }

    public static void setOpcionOC(int aOpcionOC) {
        opcionOC = aOpcionOC;
    }

    public static int getOpcionHC() {
        return opcionHC;
    }

    public static void setOpcionHC(int aOpcionHC) {
        opcionHC = aOpcionHC;
    }

    public static int getOpcionAUX() {
        return opcionAUX;
    }

    public static void setOpcionAUX(int aOpcionAUX) {
        opcionAUX = aOpcionAUX;
    }

    public static int getOpcionTEC() {
        return opcionTEC;
    }

    public static void setOpcionTEC(int aOpcionTEC) {
        opcionTEC = aOpcionTEC;
    }

    public static int getOpcionPROF() {
        return opcionPROF;
    }

    public static void setOpcionPROF(int aOpcionPROF) {
        opcionPROF = aOpcionPROF;
    }

    public static int getOpcionOPS() {
        return opcionOPS;
    }

    public static void setOpcionOPS(int aOpcionOPS) {
        opcionOPS = aOpcionOPS;
    }

    public int getOpcionDC() {
        return opcionDC;
    }

    public void setOpcionDC(int opcionDC) {
        this.opcionDC = opcionDC;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        AdministrativoAuxiliar adminAux = new AdministrativoAuxiliar();
        AdministrativoOPS adminOPS = new AdministrativoOPS();
        AdministrativoProfesional adminPro = new AdministrativoProfesional();
        AdministrativoTecnico adminTec = new AdministrativoTecnico();
        DocenteHoraCatedra docenteHC = new DocenteHoraCatedra();
        DocenteOcasional docenteOC = new DocenteOcasional();
        DocenteTiempoCompleto docenteTC = new DocenteTiempoCompleto();
        int opcion = 0;
        do {
            System.out.println("Elija las opciones de la 1 a 7:");
            System.out.println("1. Mostrar docentes de tiempo completo");
            System.out.println("2. Mostrar docentes ocasionales");
            System.out.println("3. Mostrar docentes de cátedra");
            System.out.println("4. Mostrar administrativos auxiliares");
            System.out.println("5. Mostrar administrativos técnicos");
            System.out.println("6. Mostrar administrativos profesionales");
            System.out.println("7. Mostrar administrativos OPS");
            System.out.println("Si desea terminar con el programa, ingrese un número negativo");
            opcion = teclado.nextInt();
            switch (opcion) {
                case 1: //Docente tiempo completo
                    System.out.println(docenteTC.imprimirEncabezadoLista());
                    System.out.println(docenteTC.mostrarDatos());
                    do {
                        System.out.println("Elija el docente de tiempo completo (1 a 4) para mostrar sus respectivos cálculos o digite un número negativo si desea volver al menú anterior");
                        System.out.print(docenteTC.mostrarNombres());
                        opcionDC = teclado.nextInt();
                        switch (opcionDC) {
                            case 1:
                                System.out.println("Salario base: " + docenteTC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteTC.mostrarEPS());
                                System.out.println("Pensión: " + docenteTC.mostrarEPS());
                                System.out.println("ARL: " + docenteTC.mostrarARL());
                                System.out.println("SENA: " + docenteTC.mostrarSena());
                                System.out.println("Cajas: " + docenteTC.mostrarCajas());
                                System.out.println("ICBF: " + docenteTC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteTC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteTC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteTC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                            case 2:
                                System.out.println("Salario base: " + docenteTC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteTC.mostrarEPS());
                                System.out.println("Pensión: " + docenteTC.mostrarEPS());
                                System.out.println("ARL: " + docenteTC.mostrarARL());
                                System.out.println("SENA: " + docenteTC.mostrarSena());
                                System.out.println("Cajas: " + docenteTC.mostrarCajas());
                                System.out.println("ICBF: " + docenteTC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteTC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteTC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteTC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                            case 3:
                                System.out.println("Salario base: " + docenteTC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteTC.mostrarEPS());
                                System.out.println("Pensión: " + docenteTC.mostrarEPS());
                                System.out.println("ARL: " + docenteTC.mostrarARL());
                                System.out.println("SENA: " + docenteTC.mostrarSena());
                                System.out.println("Cajas: " + docenteTC.mostrarCajas());
                                System.out.println("ICBF: " + docenteTC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteTC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteTC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteTC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                            case 4:
                                System.out.println("Salario base: " + docenteTC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteTC.mostrarEPS());
                                System.out.println("Pensión: " + docenteTC.mostrarEPS());
                                System.out.println("ARL: " + docenteTC.mostrarARL());
                                System.out.println("SENA: " + docenteTC.mostrarSena());
                                System.out.println("Cajas: " + docenteTC.mostrarCajas());
                                System.out.println("ICBF: " + docenteTC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteTC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteTC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteTC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                        }
//                    while (opcionDC > 0) {
//                        System.out.print(docenteTC.mostrarNombres());
//                        opcionDC = teclado.nextInt();
//                    }         
                    } while (opcionDC > 0);
                    break;
                case 2: //Docente ocasional
                    System.out.println(docenteOC.imprimirEncabezadoLista());
                    System.out.println(docenteOC.mostrarDatos());
                    do {
                        System.out.println("Elija el docente ocasional (1 a 4) para mostrar sus respectivos cálculos o digite un número negativo si desea volver al menú anterior ");
                        System.out.print(docenteOC.mostrarNombres());
                        opcionOC = teclado.nextInt();
                        switch (opcionOC) {
                            case 1:
                                System.out.println("Salario base: " + docenteOC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteOC.mostrarEPS());
                                System.out.println("Pensión: " + docenteOC.mostrarPension());
                                System.out.println("ARL: " + docenteOC.mostrarARL());
                                System.out.println("SENA: " + docenteOC.mostrarSena());
                                System.out.println("Cajas: " + docenteOC.mostrarCajas());
                                System.out.println("ICBF: " + docenteOC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteOC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteOC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteOC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                            case 2:
                                System.out.println("Salario base: " + docenteOC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteOC.mostrarEPS());
                                System.out.println("Pensión: " + docenteOC.mostrarPension());
                                System.out.println("ARL: " + docenteOC.mostrarARL());
                                System.out.println("SENA: " + docenteOC.mostrarSena());
                                System.out.println("Cajas: " + docenteOC.mostrarCajas());
                                System.out.println("ICBF: " + docenteOC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteOC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteOC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteOC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                            case 3:
                                System.out.println("Salario base: " + docenteOC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteOC.mostrarEPS());
                                System.out.println("Pensión: " + docenteOC.mostrarPension());
                                System.out.println("ARL: " + docenteOC.mostrarARL());
                                System.out.println("SENA: " + docenteOC.mostrarSena());
                                System.out.println("Cajas: " + docenteOC.mostrarCajas());
                                System.out.println("ICBF: " + docenteOC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteOC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteOC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteOC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                            case 4:
                                System.out.println("Salario base: " + docenteOC.mostrarSalarioBase());
                                System.out.println("EPS: " + docenteOC.mostrarEPS());
                                System.out.println("Pensión: " + docenteOC.mostrarPension());
                                System.out.println("ARL: " + docenteOC.mostrarARL());
                                System.out.println("SENA: " + docenteOC.mostrarSena());
                                System.out.println("Cajas: " + docenteOC.mostrarCajas());
                                System.out.println("ICBF: " + docenteOC.mostrarICBF());
                                System.out.println("Auxilio de transporte: " + docenteOC.mostrarAuxilio());
                                System.out.println("Salario neto: " + docenteOC.mostrarSalarioNeto());
                                System.out.println("Liquidación: " + docenteOC.mostrarLiquidacion());
                                System.out.println("");
                                break;
                        }
                    } while (opcionOC > 0);
                    break;
                case 3: //Docente de cátedra
                    System.out.println(docenteHC.imprimirEncabezadoLista());
                    System.out.println(docenteHC.mostrarDatos());
                    do{
                    System.out.println("Elija el docente de cátedra (1 a 4) para mostrar sus respectivos cálculos o digite un número negativo si desea volver al menú anterior");
                    System.out.print(docenteHC.mostrarNombres());
                    opcionHC = teclado.nextInt();
                    switch (opcionHC) {
                        case 1:
                            System.out.println("Salario base: " + docenteHC.mostrarSalarioBase());
                            System.out.println("EPS: " + docenteHC.mostrarEPS());
                            System.out.println("Pensión: " + docenteHC.mostrarPension());
                            System.out.println("ARL: " + docenteHC.mostrarARL());
                            System.out.println("SENA: " + docenteHC.mostrarSena());
                            System.out.println("Cajas: " + docenteHC.mostrarCajas());
                            System.out.println("ICBF: " + docenteHC.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + docenteHC.mostrarAuxilio());
                            System.out.println("Salario neto: " + docenteHC.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + docenteHC.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 2:
                            System.out.println("Salario base: " + docenteHC.mostrarSalarioBase());
                            System.out.println("EPS: " + docenteHC.mostrarEPS());
                            System.out.println("Pensión: " + docenteHC.mostrarPension());
                            System.out.println("ARL: " + docenteHC.mostrarARL());
                            System.out.println("SENA: " + docenteHC.mostrarSena());
                            System.out.println("Cajas: " + docenteHC.mostrarCajas());
                            System.out.println("ICBF: " + docenteHC.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + docenteHC.mostrarAuxilio());
                            System.out.println("Salario neto: " + docenteHC.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + docenteHC.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 3:
                            System.out.println("Salario base: " + docenteHC.mostrarSalarioBase());
                            System.out.println("EPS: " + docenteHC.mostrarEPS());
                            System.out.println("Pensión: " + docenteHC.mostrarPension());
                            System.out.println("ARL: " + docenteHC.mostrarARL());
                            System.out.println("SENA: " + docenteHC.mostrarSena());
                            System.out.println("Cajas: " + docenteHC.mostrarCajas());
                            System.out.println("ICBF: " + docenteHC.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + docenteHC.mostrarAuxilio());
                            System.out.println("Salario neto: " + docenteHC.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + docenteHC.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 4:
                            System.out.println("Salario base: " + docenteHC.mostrarSalarioBase());
                            System.out.println("EPS: " + docenteHC.mostrarEPS());
                            System.out.println("Pensión: " + docenteHC.mostrarPension());
                            System.out.println("ARL: " + docenteHC.mostrarARL());
                            System.out.println("SENA: " + docenteHC.mostrarSena());
                            System.out.println("Cajas: " + docenteHC.mostrarCajas());
                            System.out.println("ICBF: " + docenteHC.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + docenteHC.mostrarAuxilio());
                            System.out.println("Salario neto: " + docenteHC.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + docenteHC.mostrarLiquidacion());
                            System.out.println("");
                            break;
                    }
                    }while(opcionHC>0);
                    break;
                case 4: //Administrativo auxiliar
                    System.out.println(adminAux.imprimirEncabezadoLista());
                    System.out.println(adminAux.mostrarDatos());
                    do{
                    System.out.println("Elija el administrativo auxiliar (1 a 4) para mostrar sus respectivos cálculos o digite un número negativo si desea volver al menú anterior");
                    System.out.print(adminAux.mostrarNombres());
                    opcionAUX = teclado.nextInt();
                    switch (opcionAUX) {
                        case 1:
                            System.out.println("Salario base: " + adminAux.mostrarSalarioBase());
                            System.out.println("EPS: " + adminAux.mostrarEPS());
                            System.out.println("Pensión: " + adminAux.mostrarPension());
                            System.out.println("ARL: " + adminAux.mostrarARL());
                            System.out.println("SENA: " + adminAux.mostrarSena());
                            System.out.println("Cajas: " + adminAux.mostrarCajas());
                            System.out.println("ICBF: " + adminAux.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminAux.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminAux.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminAux.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 2:
                            System.out.println("Salario base: " + adminAux.mostrarSalarioBase());
                            System.out.println("EPS: " + adminAux.mostrarEPS());
                            System.out.println("Pensión: " + adminAux.mostrarPension());
                            System.out.println("ARL: " + adminAux.mostrarARL());
                            System.out.println("SENA: " + adminAux.mostrarSena());
                            System.out.println("Cajas: " + adminAux.mostrarCajas());
                            System.out.println("ICBF: " + adminAux.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminAux.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminAux.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminAux.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 3:
                            System.out.println("Salario base: " + adminAux.mostrarSalarioBase());
                            System.out.println("EPS: " + adminAux.mostrarEPS());
                            System.out.println("Pensión: " + adminAux.mostrarPension());
                            System.out.println("ARL: " + adminAux.mostrarARL());
                            System.out.println("SENA: " + adminAux.mostrarSena());
                            System.out.println("Cajas: " + adminAux.mostrarCajas());
                            System.out.println("ICBF: " + adminAux.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminAux.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminAux.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminAux.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 4:
                            System.out.println("Salario base: " + adminAux.mostrarSalarioBase());
                            System.out.println("EPS: " + adminAux.mostrarEPS());
                            System.out.println("Pensión: " + adminAux.mostrarPension());
                            System.out.println("ARL: " + adminAux.mostrarARL());
                            System.out.println("SENA: " + adminAux.mostrarSena());
                            System.out.println("Cajas: " + adminAux.mostrarCajas());
                            System.out.println("ICBF: " + adminAux.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminAux.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminAux.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminAux.mostrarLiquidacion());
                            System.out.println("");
                            break;
                    }
                    }while(opcionAUX > 0);
                    break;
                case 5: //Administrativo técnico
                    System.out.println(adminTec.imprimirEncabezadoLista());
                    System.out.println(adminTec.mostrarDatos());
                    do{
                    System.out.println("Elija el administrativo técnico (1 a 4) para mostrar sus respectivos cálculos o digite un número negativo si desea volver al menú anterior");
                    System.out.print(adminTec.mostrarNombres());
                    opcionTEC = teclado.nextInt();
                    switch (opcionTEC) {
                        case 1:
                            System.out.println("Salario base: " + adminTec.mostrarSalarioBase());
                            System.out.println("EPS: " + adminTec.mostrarEPS());
                            System.out.println("Pensión: " + adminTec.mostrarPension());
                            System.out.println("ARL: " + adminTec.mostrarARL());
                            System.out.println("SENA: " + adminTec.mostrarSena());
                            System.out.println("Cajas: " + adminTec.mostrarCajas());
                            System.out.println("ICBF: " + adminTec.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminTec.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminTec.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminTec.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 2:
                            System.out.println("Salario base: " + adminTec.mostrarSalarioBase());
                            System.out.println("EPS: " + adminTec.mostrarEPS());
                            System.out.println("Pensión: " + adminTec.mostrarPension());
                            System.out.println("ARL: " + adminTec.mostrarARL());
                            System.out.println("SENA: " + adminTec.mostrarSena());
                            System.out.println("Cajas: " + adminTec.mostrarCajas());
                            System.out.println("ICBF: " + adminTec.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminTec.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminTec.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminTec.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 3:
                            System.out.println("Salario base: " + adminTec.mostrarSalarioBase());
                            System.out.println("EPS: " + adminTec.mostrarEPS());
                            System.out.println("Pensión: " + adminTec.mostrarPension());
                            System.out.println("ARL: " + adminTec.mostrarARL());
                            System.out.println("SENA: " + adminTec.mostrarSena());
                            System.out.println("Cajas: " + adminTec.mostrarCajas());
                            System.out.println("ICBF: " + adminTec.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminTec.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminTec.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminTec.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 4:
                            System.out.println("Salario base: " + adminTec.mostrarSalarioBase());
                            System.out.println("EPS: " + adminTec.mostrarEPS());
                            System.out.println("Pensión: " + adminTec.mostrarPension());
                            System.out.println("ARL: " + adminTec.mostrarARL());
                            System.out.println("SENA: " + adminTec.mostrarSena());
                            System.out.println("Cajas: " + adminTec.mostrarCajas());
                            System.out.println("ICBF: " + adminTec.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminTec.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminTec.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminTec.mostrarLiquidacion());
                            System.out.println("");
                            break;
                    }
                    }while(opcionTEC > 0);
                    break;
                case 6: //Administrativo profesional
                    System.out.println(adminPro.imprimirEncabezadoLista());
                    System.out.println(adminPro.mostrarDatos());
                    do{
                    System.out.println("Elija el administrativo profesional (1 a 4) para mostrar sus respectivos cálculos o digite un número negativo si desea volver al menú anterior");
                    System.out.print(adminPro.mostrarNombres());
                    opcionPROF = teclado.nextInt();
                    switch (opcionPROF) {
                        case 1:
                            System.out.println("Salario base: " + adminPro.mostrarSalarioBase());
                            System.out.println("EPS: " + adminPro.mostrarEPS());
                            System.out.println("Pensión: " + adminPro.mostrarPension());
                            System.out.println("ARL: " + adminPro.mostrarARL());
                            System.out.println("SENA: " + adminPro.mostrarSena());
                            System.out.println("Cajas: " + adminPro.mostrarCajas());
                            System.out.println("ICBF: " + adminPro.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminPro.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminPro.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminPro.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 2:
                            System.out.println("Salario base: " + adminPro.mostrarSalarioBase());
                            System.out.println("EPS: " + adminPro.mostrarEPS());
                            System.out.println("Pensión: " + adminPro.mostrarPension());
                            System.out.println("ARL: " + adminPro.mostrarARL());
                            System.out.println("SENA: " + adminPro.mostrarSena());
                            System.out.println("Cajas: " + adminPro.mostrarCajas());
                            System.out.println("ICBF: " + adminPro.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminPro.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminPro.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminPro.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 3:
                            System.out.println("Salario base: " + adminPro.mostrarSalarioBase());
                            System.out.println("EPS: " + adminPro.mostrarEPS());
                            System.out.println("Pensión: " + adminPro.mostrarPension());
                            System.out.println("ARL: " + adminPro.mostrarARL());
                            System.out.println("SENA: " + adminPro.mostrarSena());
                            System.out.println("Cajas: " + adminPro.mostrarCajas());
                            System.out.println("ICBF: " + adminPro.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminPro.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminPro.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminPro.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 4:
                            System.out.println("Salario base: " + adminPro.mostrarSalarioBase());
                            System.out.println("EPS: " + adminPro.mostrarEPS());
                            System.out.println("Pensión: " + adminPro.mostrarPension());
                            System.out.println("ARL: " + adminPro.mostrarARL());
                            System.out.println("SENA: " + adminPro.mostrarSena());
                            System.out.println("Cajas: " + adminPro.mostrarCajas());
                            System.out.println("ICBF: " + adminPro.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminPro.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminPro.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminPro.mostrarLiquidacion());
                            System.out.println("");
                            break;
                    }
                    }while(opcionPROF > 0);
                    break;
                case 7: //Administrativo OPS
                    System.out.println(adminOPS.imprimirEncabezadoLista());
                    System.out.println(adminOPS.mostrarDatos());
                    do{
                    System.out.println("Elija el administrativo OPS (1 a 4) para mostrar sus respectivos cálculos o digite un número negativo si desea volver al menú anterior");
                    System.out.print(adminOPS.mostrarNombres());
                    opcionOPS = teclado.nextInt();
                    switch (opcionOPS) {
                        case 1:
                            System.out.println("Salario base: " + adminOPS.mostrarSalarioBase());
                            System.out.println("EPS: " + adminOPS.mostrarEPS());
                            System.out.println("Pensión: " + adminOPS.mostrarPension());
                            System.out.println("ARL: " + adminOPS.mostrarARL());
                            System.out.println("SENA: " + adminOPS.mostrarSena());
                            System.out.println("Cajas: " + adminOPS.mostrarCajas());
                            System.out.println("ICBF: " + adminOPS.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminOPS.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminOPS.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminOPS.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 2:
                            System.out.println("Salario base: " + adminOPS.mostrarSalarioBase());
                            System.out.println("EPS: " + adminOPS.mostrarEPS());
                            System.out.println("Pensión: " + adminOPS.mostrarPension());
                            System.out.println("ARL: " + adminOPS.mostrarARL());
                            System.out.println("SENA: " + adminOPS.mostrarSena());
                            System.out.println("Cajas: " + adminOPS.mostrarCajas());
                            System.out.println("ICBF: " + adminOPS.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminOPS.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminOPS.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminOPS.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 3:
                            System.out.println("Salario base: " + adminOPS.mostrarSalarioBase());
                            System.out.println("EPS: " + adminOPS.mostrarEPS());
                            System.out.println("Pensión: " + adminOPS.mostrarPension());
                            System.out.println("ARL: " + adminOPS.mostrarARL());
                            System.out.println("SENA: " + adminOPS.mostrarSena());
                            System.out.println("Cajas: " + adminOPS.mostrarCajas());
                            System.out.println("ICBF: " + adminOPS.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminOPS.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminOPS.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminOPS.mostrarLiquidacion());
                            System.out.println("");
                            break;
                        case 4:
                            System.out.println("Salario base: " + adminOPS.mostrarSalarioBase());
                            System.out.println("EPS: " + adminOPS.mostrarEPS());
                            System.out.println("Pensión: " + adminOPS.mostrarPension());
                            System.out.println("ARL: " + adminOPS.mostrarARL());
                            System.out.println("SENA: " + adminOPS.mostrarSena());
                            System.out.println("Cajas: " + adminOPS.mostrarCajas());
                            System.out.println("ICBF: " + adminOPS.mostrarICBF());
                            System.out.println("Auxilio de transporte: " + adminOPS.mostrarAuxilio());
                            System.out.println("Salario neto: " + adminOPS.mostrarSalarioNeto());
                            System.out.println("Liquidación: " + adminOPS.mostrarLiquidacion());
                            System.out.println("");
                            break;
                    }
                    }while(opcionOPS > 0);
                    break;
            }
        } while (opcion >= 0);

    }

}
