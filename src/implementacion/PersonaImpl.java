/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacion;

/**
 *
 * @author andre
 */
public interface PersonaImpl {
    public String mostrarDG();
    public double calculaEPS(double salario);
    public double calculaPension(double salario);
    public double calculaARL(double salario);
    public double calculaSENA(double salario);
    public double calculaCajas(double salario);
    public double calculaICBF(double salario);
    public double calculaAuxilio(double salario);
}
