/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacion.tipoDocente;

/**
 *
 * @author andre
 */
public interface Ocasional {
    public double calcularSueldo();
    public double liquidarOC();
    public String mostrarLiqOC();
}
