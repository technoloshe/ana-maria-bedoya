/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacion.administrativo.planta;

/**
 *
 * @author andre
 */
public interface Tecnico {
    public double calcularSueldo();
    public double liquidarTec();
    public String mostrarLiqTec();
}
