/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacion.administrativo;

/**
 *
 * @author andre
 */
public interface OPS {
    public double calcularSueldo();
    public double liquidarValorContrato();
    public String mostrarLiqOPS();
}
