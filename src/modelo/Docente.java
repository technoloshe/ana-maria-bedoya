/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import implementacion.DocenteImpl;

/**
 *
 * @author andre
 */
public class Docente extends Persona implements DocenteImpl {
    private String areaFormacion;
    private String tituloProfesional;
    private String unidadAcademica;

    public Docente() {
    }

    public Docente(String areaFormacion, String tituloProfesional, String unidadAcademica) {
        this.areaFormacion = areaFormacion;
        this.tituloProfesional = tituloProfesional;
        this.unidadAcademica = unidadAcademica;
    }

    public Docente(String areaFormacion, String tituloProfesional, String unidadAcademica, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.areaFormacion = areaFormacion;
        this.tituloProfesional = tituloProfesional;
        this.unidadAcademica = unidadAcademica;
    }

    public String getUnidadAcademica() {
        return unidadAcademica;
    }

    public void setUnidadAcademica(String unidadAcademica) {
        this.unidadAcademica = unidadAcademica;
    }

    public String getAreaFormacion() {
        return areaFormacion;
    }

    public void setAreaFormacion(String areaFormacion) {
        this.areaFormacion = areaFormacion;
    }

    public String getTituloProfesional() {
        return tituloProfesional;
    }

    public void setTituloProfesional(String tituloProfesional) {
        this.tituloProfesional = tituloProfesional;
    }

    @Override
    public String toString() {
        return "Docente{" + "areaFormacion=" + areaFormacion + ", tituloProfesional=" + tituloProfesional + ", unidadAcademica=" + unidadAcademica + '}';
    }
    
    public String mostrarDatosDocente(){
        return super.getIdPersona() + "\t" + super.getNomPersona() + "\t" + "   " + super.getApePersona() + "\t" + super.getFechaNacimiento() + "\t" + "        " + super.getGenero() + "\t" + "      " + areaFormacion  + "\t" + "     " + tituloProfesional + "\t" + "  " +unidadAcademica;
    }
}
