/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tipoDocente;

import modelo.Docente;

/**
 *
 * @author andre
 */
public class TiempoCompleto extends Docente implements implementacion.tipoDocente.TiempoCompleto {

    private String categoria;
    private int puntos;
    private double salario;
    private int numMeses;

    public TiempoCompleto() {
    }

    public TiempoCompleto(String categoria, int puntos, double salario, int numMeses) {
        this.categoria = categoria;
        this.puntos = puntos;
        this.salario = salario;
        this.numMeses = numMeses;
    }

    public TiempoCompleto(String categoria, int puntos, double salario, int numMeses, String areaFormacion, String tituloProfesional, String unidadAcademica) {
        super(areaFormacion, tituloProfesional, unidadAcademica);
        this.categoria = categoria;
        this.puntos = puntos;
        this.salario = salario;
        this.numMeses = numMeses;
    }

    public TiempoCompleto(String categoria, int puntos, double salario, int numMeses, String areaFormacion, String tituloProfesional, String unidadAcademica, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(areaFormacion, tituloProfesional, unidadAcademica, idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.categoria = categoria;
        this.puntos = puntos;
        this.salario = salario;
        this.numMeses = numMeses;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    
    public int getNumMeses() {
        return numMeses;
    }

    public void setNumMeses(int numMeses) {
        this.numMeses = numMeses;
    }

    @Override
    public String toString() {
        return "TiempoCompleto{" + "categoria=" + categoria + ", puntos=" + puntos + ", salario=" + salario + ", numMeses=" + numMeses + '}';
    }
    
    @Override
    public double calcularSueldo() {
        double salarioNeto = 0;
        salarioNeto = salario + (super.calculaAuxilio(salario)) - (super.calculaEPS(salario)) - super.calculaPension(salario) - (super.calculaSENA(salario)) - (super.calculaCajas(salario)) - (super.calculaICBF(salario));        
        return salarioNeto;
    }

    @Override
    public double liquidarTC() {
        double liquidacionTC = 0;
        liquidacionTC = calcularSueldo() * numMeses;
        return liquidacionTC;
    }

    @Override
    public String mostrarLiqTC() {
        return "Liquidación docente de tiempo completo: "+liquidarTC();
    }

}
