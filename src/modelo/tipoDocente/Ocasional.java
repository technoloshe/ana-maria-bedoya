/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tipoDocente;

import modelo.Docente;

/**
 *
 * @author andre
 */
public class Ocasional extends Docente implements implementacion.tipoDocente.Ocasional {

    private String ultimoTitulo;
    private int numMeses;
    private double salario;

    public Ocasional() {
    }

    public Ocasional(String ultimoTitulo, int numMeses, double salario) {
        this.ultimoTitulo = ultimoTitulo;
        this.numMeses = numMeses;
        this.salario = salario;
    }

    public Ocasional(String ultimoTitulo, int numMeses, double salario, String areaFormacion, String tituloProfesional, String unidadAcademica) {
        super(areaFormacion, tituloProfesional, unidadAcademica);
        this.ultimoTitulo = ultimoTitulo;
        this.numMeses = numMeses;
        this.salario = salario;
    }

    public Ocasional(String ultimoTitulo, int numMeses, double salario, String areaFormacion, String tituloProfesional, String unidadAcademica, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(areaFormacion, tituloProfesional, unidadAcademica, idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.ultimoTitulo = ultimoTitulo;
        this.numMeses = numMeses;
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getUltimoTitulo() {
        return ultimoTitulo;
    }

    public void setUltimoTitulo(String ultimoTitulo) {
        this.ultimoTitulo = ultimoTitulo;
    }

    public int getNumMeses() {
        return numMeses;
    }

    public void setNumMeses(int numMeses) {
        this.numMeses = numMeses;
    }

    @Override
    public String toString() {
        return "Ocasional{" + "ultimoTitulo=" + ultimoTitulo + ", numMeses=" + numMeses + ", salario=" + salario + '}';
    }
    
    @Override
    public double calcularSueldo() {
        double salarioNeto = 0;
        salarioNeto = salario + (super.calculaAuxilio(salario)) - (super.calculaEPS(salario)) - super.calculaPension(salario) - (super.calculaSENA(salario)) - (super.calculaCajas(salario)) - (super.calculaICBF(salario));        
        return salarioNeto;
    }

    @Override
    public double liquidarOC() {
        double liquidacionOC = 0;
        liquidacionOC = calcularSueldo() * numMeses;
        return liquidacionOC;
    }

    @Override
    public String mostrarLiqOC() {
        return "Liquidación docente ocasional: "+liquidarOC();
    }

}
