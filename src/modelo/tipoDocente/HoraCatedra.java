/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tipoDocente;

import modelo.Docente;

/**
 *
 * @author andre
 */
public class HoraCatedra extends Docente implements implementacion.tipoDocente.HoraCatedra {

    private String ultimoTitulo;
    private int numHoras;
    private double valorContrato;
    private double salario;

    public HoraCatedra() {
    }

    public HoraCatedra(String ultimoTitulo, int numHoras, double valorContrato, double salario) {
        this.ultimoTitulo = ultimoTitulo;
        this.numHoras = numHoras;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

    public HoraCatedra(String ultimoTitulo, int numHoras, double valorContrato, double salario, String areaFormacion, String tituloProfesional, String unidadAcademica) {
        super(areaFormacion, tituloProfesional, unidadAcademica);
        this.ultimoTitulo = ultimoTitulo;
        this.numHoras = numHoras;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

    public HoraCatedra(String ultimoTitulo, int numHoras, double valorContrato, double salario, String areaFormacion, String tituloProfesional, String unidadAcademica, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(areaFormacion, tituloProfesional, unidadAcademica, idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.ultimoTitulo = ultimoTitulo;
        this.numHoras = numHoras;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getUltimoTitulo() {
        return ultimoTitulo;
    }

    public void setUltimoTitulo(String ultimoTitulo) {
        this.ultimoTitulo = ultimoTitulo;
    }

    public int getNumHoras() {
        return numHoras;
    }

    public void setNumHoras(int numHoras) {
        this.numHoras = numHoras;
    }

    public double getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(double valorContrato) {
        this.valorContrato = valorContrato;
    }

    @Override
    public String toString() {
        return "HoraCatedra{" + "ultimoTitulo=" + ultimoTitulo + ", numHoras=" + numHoras + ", valorContrato=" + valorContrato + ", salario=" + salario + '}';
    }
    
    @Override
    public double calcularSueldo() {
        double salarioNeto = 0;
        salarioNeto = salario + (super.calculaAuxilio(salario)) - (super.calculaEPS(salario)) - super.calculaPension(salario) - (super.calculaSENA(salario)) - (super.calculaCajas(salario)) - (super.calculaICBF(salario));        
        return salarioNeto;
    }

    @Override
    public double liquidarHC() {
        double liquidacionHC = 0;
        liquidacionHC = calcularSueldo() * numHoras;
        return liquidacionHC;
    }

    @Override
    public String mostrarLiqHC() {
        return "Liquidación docente de cátedra: "+liquidarHC();
    }

}
