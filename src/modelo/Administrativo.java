/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import implementacion.AdministrativoImpl;

/**
 *
 * @author andre
 */
public class Administrativo extends Persona implements AdministrativoImpl {
    private String dependencia;
    private String titulo;

    public Administrativo() {
    }

    public Administrativo(String dependencia, String titulo) {
        this.dependencia = dependencia;
        this.titulo = titulo;
    }

    public Administrativo(String dependencia, String titulo, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.dependencia = dependencia;
        this.titulo = titulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    @Override
    public String toString() {
        return "Administrativo{" + "dependencia=" + dependencia + ", titulo=" + titulo + '}';
    }
    
    public String mostrarDatosAdmin(){
        return super.getIdPersona() + "\t" + super.getNomPersona() + "\t" + "   " + super.getApePersona() + "\t" + super.getFechaNacimiento() + "\t" + "        " + super.getGenero() + "\t" + "      " + dependencia + "\t" + "     " + titulo;
    }
    
}
