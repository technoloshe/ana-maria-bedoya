/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.administrativo.planta;

import modelo.administrativo.Planta;

/**
 *
 * @author andre
 */
public class Profesional extends Planta implements implementacion.administrativo.planta.Profesional{
    private int nivel;
    private double salario;

    public Profesional() {
    }

    public Profesional(int nivel, double salario) {
        this.nivel = nivel;
        this.salario = salario;
    }

    public Profesional(int nivel, double salario, String fechaVinculacion, int numMeses, String dependencia, String titulo) {
        super(fechaVinculacion, numMeses, dependencia, titulo);
        this.nivel = nivel;
        this.salario = salario;
    }

    public Profesional(int nivel, double salario, String fechaVinculacion, int numMeses, String dependencia, String titulo, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(fechaVinculacion, numMeses, dependencia, titulo, idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.nivel = nivel;
        this.salario = salario;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    @Override
    public double calcularSueldo() {
        double salarioNeto = 0;
        salarioNeto = salario + (super.calculaAuxilio(salario)) - (super.calculaEPS(salario)) - super.calculaPension(salario) - (super.calculaSENA(salario)) - (super.calculaCajas(salario)) - (super.calculaICBF(salario));        
        return salarioNeto;
    }

    @Override
    public double liquidarProf() {
        double liquidacionProf = 0;
        liquidacionProf = calcularSueldo() * super.getNumMeses();
        return liquidacionProf;
    }

    @Override
    public String mostrarLiqProf() {
        return "Liquidación de profesional: "+liquidarProf();
    }
    
    
}
