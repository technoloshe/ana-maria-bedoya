/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.administrativo;

import modelo.Administrativo;

/**
 *
 * @author andre
 */
public class Planta extends Administrativo implements implementacion.administrativo.PlantaImpl{
    private String fechaVinculacion;
    private int numMeses;

    public Planta() {
    }

    public Planta(String fechaVinculacion, int numMeses) {
        this.fechaVinculacion = fechaVinculacion;
        this.numMeses = numMeses;
    }

    public Planta(String fechaVinculacion, int numMeses, String dependencia, String titulo) {
        super(dependencia, titulo);
        this.fechaVinculacion = fechaVinculacion;
        this.numMeses = numMeses;
    }

    public Planta(String fechaVinculacion, int numMeses, String dependencia, String titulo, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(dependencia, titulo, idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.fechaVinculacion = fechaVinculacion;
        this.numMeses = numMeses;
    }

    public String getFechaVinculacion() {
        return fechaVinculacion;
    }

    public void setFechaVinculacion(String fechaVinculacion) {
        this.fechaVinculacion = fechaVinculacion;
    }
    
    public int getNumMeses() {
        return numMeses;
    }

    public void setNumMeses(int numMeses) {
        this.numMeses = numMeses;
    }

    @Override
    public String toString() {
        return "Planta{" + "fechaVinculacion=" + fechaVinculacion + '}';
    }

    @Override
    public void mostrarDatosAdminPlanta() {
        System.out.println("ID: " + super.getIdPersona() + " " + "Nombre: " + super.getNomPersona() + " " + "Apellido: " + super.getApePersona() + " " + "Fecha de nacimiento: " + super.getFechaNacimiento() + " " + "Ciudad de nacimiento: " + super.getCiudadNacimiento() + " " + "Género: " + super.getGenero() + " " + "Estrato: " + super.getEstrato() + " " + "Fecha de vinculación: " + fechaVinculacion);
    }

}
