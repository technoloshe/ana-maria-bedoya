/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.administrativo;

import modelo.Administrativo;

/**
 *
 * @author andre
 */
public class OPS extends Administrativo implements implementacion.administrativo.OPS {

    private String fechaVinculacion;
    private int numMeses;
    private int valorContrato;
    private double salario;

    public OPS() {
    }

    public OPS(String fechaVinculacion, int numMeses, int valorContrato, double salario) {
        this.fechaVinculacion = fechaVinculacion;
        this.numMeses = numMeses;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

    public OPS(String fechaVinculacion, int numMeses, int valorContrato, double salario, String dependencia, String titulo) {
        super(dependencia, titulo);
        this.fechaVinculacion = fechaVinculacion;
        this.numMeses = numMeses;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

    public OPS(String fechaVinculacion, int numMeses, int valorContrato, double salario, String dependencia, String titulo, int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        super(dependencia, titulo, idPersona, nomPersona, apePersona, fechaNacimiento, ciudadNacimiento, genero, estrato, tipoRiesgo);
        this.fechaVinculacion = fechaVinculacion;
        this.numMeses = numMeses;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

    public String getFechaVinculacion() {
        return fechaVinculacion;
    }

    public void setFechaVinculacion(String fechaVinculacion) {
        this.fechaVinculacion = fechaVinculacion;
    }

    public int getNumMeses() {
        return numMeses;
    }

    public void setNumMeses(int numMeses) {
        this.numMeses = numMeses;
    }

    public int getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(int valorContrato) {
        this.valorContrato = valorContrato;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    @Override
    public double calcularSueldo() {
        double salarioNeto = 0;
        salarioNeto = salario + (super.calculaAuxilio(salario)) - (super.calculaEPS(salario)) - super.calculaPension(salario) - (super.calculaSENA(salario)) - (super.calculaCajas(salario)) - (super.calculaICBF(salario));        
        return salarioNeto;
    }

    @Override
    public double liquidarValorContrato() {
        double liquidacionOPS = 0;
        liquidacionOPS = calcularSueldo() * numMeses;
        return liquidacionOPS;
    }

    @Override
    public String mostrarLiqOPS() {
        return "Liquidación de OPS: "+liquidarValorContrato();
    }

}
