/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import implementacion.PersonaImpl;

/**
 *
 * @author andre
 */
public class Persona implements PersonaImpl {

    private int idPersona;
    private String nomPersona;
    private String apePersona;
    private String fechaNacimiento;
    private String ciudadNacimiento;
    private String genero;
    private int estrato;
    private String tipoRiesgo;

    public Persona() {
    }

    public Persona(int idPersona, String nomPersona, String apePersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato, String tipoRiesgo) {
        this.idPersona = idPersona;
        this.nomPersona = nomPersona;
        this.apePersona = apePersona;
        this.fechaNacimiento = fechaNacimiento;
        this.ciudadNacimiento = ciudadNacimiento;
        this.genero = genero;
        this.estrato = estrato;
        this.tipoRiesgo = tipoRiesgo;
    }

    public int getEstrato() {
        return estrato;
    }

    public void setEstrato(int estrato) {
        this.estrato = estrato;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNomPersona() {
        return nomPersona;
    }

    public void setNomPersona(String nomPersona) {
        this.nomPersona = nomPersona;
    }

    public String getApePersona() {
        return apePersona;
    }

    public void setApePersona(String apePersona) {
        this.apePersona = apePersona;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCiudadNacimiento() {
        return ciudadNacimiento;
    }

    public void setCiudadNacimiento(String ciudadNacimiento) {
        this.ciudadNacimiento = ciudadNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTipoRiesgo() {
        return tipoRiesgo;
    }

    public void setTipoRiesgo(String tipoRiesgo) {
        this.tipoRiesgo = tipoRiesgo;
    }

    @Override
    public String toString() {
        return "Persona{" + "idPersona=" + idPersona + ", nomPersona=" + nomPersona + ", apePersona=" + apePersona + ", fechaNacimiento=" + fechaNacimiento + ", ciudadNacimiento=" + ciudadNacimiento + ", genero=" + genero + ", estrato=" + estrato + ", tipoRiesgo=" + tipoRiesgo + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + this.idPersona;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (this.idPersona != other.idPersona) {
            return false;
        }
        return true;
    }

    @Override
    public String mostrarDG() {
        return "ID: " + idPersona + " " + "Nombre: " + nomPersona + " " + "Apellido: " + apePersona + " " + "Fecha de nacimiento: " + fechaNacimiento + " " + "Ciudad de nacimiento: " + ciudadNacimiento + " " + "Género: " + genero + " " + "Estrato: " + estrato + " " + "Tipo riesgo: " + tipoRiesgo;
    }

    /**
     * Los porcentajes a deducir del salario se tomaron de:
     * http://www.eafit.edu.co/escuelas/administracion/departamentos/departamento-contaduria-publica/planta-docente/Documents/Nota%20de%20clase%2072%20aspectos%20sobre%20salario,%20nomina%20y%20parafiscales.pdf
     */
    @Override
    public double calculaEPS(double salario) {
        double porcentajeEPS = 0.04;
        return porcentajeEPS * salario;

    }

    @Override
    public double calculaPension(double salario) {
        double porcentajePension = 0.04;
        return porcentajePension * salario;
    }

    //La información para calcular el porcentaje para la ARL fue tomada de: https://www.gerencie.com/aportes-a-seguridad-social.html
    @Override
    public double calculaARL(double salario) {
        double porcentajeARL = 0;
        switch (tipoRiesgo) {
            case "I":
            porcentajeARL = 0.00522;
            break;
            case "II":
            porcentajeARL = 0.01044;
            break;
            case "III":
            porcentajeARL = 0.02436;
            case "IV":
            porcentajeARL = 0.04350;
            case "V":
            porcentajeARL = 0.06960;    
            break;
        }
        return porcentajeARL;
    }

    @Override
    public double calculaSENA(double salario) {
        double porcentajeSena = 0.02;
        return porcentajeSena * salario;
    }

    @Override
    public double calculaCajas(double salario) {
        double porcentajeCajas = 0.04;
        return porcentajeCajas * salario;
    }

    @Override
    public double calculaICBF(double salario) {
        double porcentajeICBF = 0.03;
        return porcentajeICBF * salario;
    }

    //La información del auxilio fue tomada de: https://www.rankia.co/blog/mejores-opiniones-colombia/3941998-subsidio-transporte-2019-pago-auxilio-valor
    @Override
    public double calculaAuxilio(double salario) {
        double montoAuxilio = 0;
        if (salario > 1656232) {
            return montoAuxilio;
        } else {
            montoAuxilio = 97032;
            return montoAuxilio;
        }
    }

}
