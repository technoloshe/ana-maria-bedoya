/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controllerImpl.Datos;
import modelo.administrativo.planta.Tecnico;
import vista.Vista;

/**
 *
 * @author ANA_MARIA
 */
public class AdministrativoTecnico implements Datos {
    Tecnico adminTec[] = new Tecnico[4];
    Vista vista = new Vista();
    
    private void llenarLista(){
        this.adminTec[0] = new Tecnico(1, 859000, "01-06-2019", 1, "Producción", "Técnico en Mantenimiento", 43567845, "John", "Hernández", "20-05-1945", "Guarne", "H", 3,"III");
        this.adminTec[1] = new Tecnico(1, 1000000, "14-02-2019", 5, "Producción", "Técnico en Mantenimiento", 968494584, "Julio", "Puentes", "20-11-1967", "Medellín", "H", 3,"III");
        this.adminTec[2] = new Tecnico(1, 1200000, "03-05-2019", 2, "Mantenimiento", "Técnico en Mantenimiento", 22734384, "Freddy", "Paredes", "04-05-1986", "Medellín", "H", 3,"III");
        this.adminTec[3] = new Tecnico(1, 850000, "01-04-2019", 3, "Mantenimiento", "Técnico en Mantenimiento", 17458374, "Daniel", "Hoyos", "10-02-1990", "Medellín", "H", 3,"III");
    }
    
     public Tecnico[] listaAdminTec() {
        llenarLista();
        return this.adminTec;
    }

    @Override
    public String imprimirEncabezadoLista() {
        return "ID      " + "\t" + "Nombre" + "\t   " + "Apellido     " + "Fecha de nacimiento" + "   "  + "Género" + "\t" + "     "  + "Dependencia" + "\t" + "     " + "Título";
    }
    
    @Override
    public String mostrarDatos() {
        for (int i = 0; i < listaAdminTec().length; i++) {
            System.out.println(adminTec[i].mostrarDatosAdmin());
        }
        return "";
    }
    
    @Override
    public String mostrarNombres(){
        for (int i = 0; i < listaAdminTec().length; i++) {
            System.out.println(i+1 + ". " + adminTec[i].getNomPersona());
        }
        return "";
    }
   

    @Override
    public double mostrarSalarioNeto() {
        double salarioNeto =0;
        switch(vista.getOpcionTEC()){
            case 1:
                salarioNeto = adminTec[0].calcularSueldo();
                break;
            case 2:
                salarioNeto = adminTec[1].calcularSueldo();
                break;
            case 3:
                salarioNeto = adminTec[2].calcularSueldo();
                break;
            case 4:
                salarioNeto = adminTec[3].calcularSueldo();
                break;
        }
        return salarioNeto;
    }
    
    @Override
    public double mostrarSalarioBase() {
        double salarioBase =0;
        switch(vista.getOpcionTEC()){
            case 1:
                salarioBase = adminTec[0].getSalario();
                break;
            case 2:
                salarioBase = adminTec[1].getSalario();
                break;
            case 3:
                salarioBase = adminTec[2].getSalario();
                break;
            case 4:
                salarioBase = adminTec[3].getSalario();
                break;
        }
        return salarioBase;
    }
    
    @Override
    public double mostrarEPS() {
        double montoEPS =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoEPS = adminTec[0].calculaEPS(adminTec[0].getSalario());
                break;
            case 2:
                montoEPS = adminTec[1].calculaEPS(adminTec[1].getSalario());
                break;
            case 3:
                montoEPS = adminTec[2].calculaEPS(adminTec[2].getSalario());
                break;
            case 4:
                montoEPS = adminTec[3].calculaEPS(adminTec[3].getSalario());
                break;
        }
        return montoEPS;
    }

    @Override
    public double mostrarPension() {
        double montoPension =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoPension = adminTec[0].calculaPension(adminTec[0].getSalario());
                break;
            case 2:
                montoPension = adminTec[1].calculaPension(adminTec[1].getSalario());
                break;
            case 3:
                montoPension = adminTec[2].calculaPension(adminTec[2].getSalario());
                break;
            case 4:
                montoPension = adminTec[3].calculaPension(adminTec[3].getSalario());
                break;
        }
        return montoPension;
    }

    @Override
    public double mostrarARL() {
        double montoARL =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoARL = adminTec[0].calculaARL(adminTec[0].getSalario());
                break;
            case 2:
                montoARL = adminTec[1].calculaARL(adminTec[1].getSalario());
                break;
            case 3:
                montoARL = adminTec[2].calculaARL(adminTec[2].getSalario());
                break;
            case 4:
                montoARL = adminTec[3].calculaARL(adminTec[3].getSalario());
                break;
        }
        return montoARL;
    }

    @Override
    public double mostrarSena() {
        double montoSENA =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoSENA = adminTec[0].calculaSENA(adminTec[0].getSalario());
                break;
            case 2:
                montoSENA = adminTec[1].calculaSENA(adminTec[1].getSalario());
                break;
            case 3:
                montoSENA = adminTec[2].calculaSENA(adminTec[2].getSalario());
                break;
            case 4:
                montoSENA = adminTec[3].calculaSENA(adminTec[3].getSalario());
                break;
        }
        return montoSENA;
    }

    @Override
    public double mostrarCajas() {
        double montoCajas =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoCajas = adminTec[0].calculaCajas(adminTec[0].getSalario());
                break;
            case 2:
                montoCajas = adminTec[1].calculaCajas(adminTec[1].getSalario());
                break;
            case 3:
                montoCajas = adminTec[2].calculaCajas(adminTec[2].getSalario());
                break;
            case 4:
                montoCajas = adminTec[3].calculaCajas(adminTec[3].getSalario());
                break;
        }
        return montoCajas;
    }

    @Override
    public double mostrarICBF() {
        double montoICBF =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoICBF = adminTec[0].calculaICBF(adminTec[0].getSalario());
                break;
            case 2:
                montoICBF = adminTec[1].calculaICBF(adminTec[1].getSalario());
                break;
            case 3:
                montoICBF = adminTec[2].calculaICBF(adminTec[2].getSalario());
                break;
            case 4:
                montoICBF = adminTec[3].calculaICBF(adminTec[3].getSalario());
                break;
        }
        return montoICBF;
    }

    @Override
    public double mostrarAuxilio() {
        double montoAuxilio =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoAuxilio = adminTec[0].calculaAuxilio(adminTec[0].getSalario());
                break;
            case 2:
                montoAuxilio = adminTec[1].calculaAuxilio(adminTec[1].getSalario());
                break;
            case 3:
                montoAuxilio = adminTec[2].calculaAuxilio(adminTec[2].getSalario());
                break;
            case 4:
                montoAuxilio = adminTec[3].calculaAuxilio(adminTec[3].getSalario());
                break;
        }
        return montoAuxilio;
    }

    @Override
    public double mostrarLiquidacion() {
        double montoLiquidacion =0;
        switch(vista.getOpcionTEC()){
            case 1:
                montoLiquidacion = adminTec[0].liquidarTec();
                break;
            case 2:
                montoLiquidacion = adminTec[1].liquidarTec();
                break;
            case 3:
                montoLiquidacion = adminTec[2].liquidarTec();
                break;
            case 4:
                montoLiquidacion = adminTec[3].liquidarTec();
                break;
        }
        return montoLiquidacion;
    }

}
