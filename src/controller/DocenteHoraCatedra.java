/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controllerImpl.Datos;
import modelo.tipoDocente.HoraCatedra;
import vista.Vista;

/**
 *
 * @author ANA_MARIA
 */
public class DocenteHoraCatedra implements Datos {
    HoraCatedra docentesHC[] = new HoraCatedra[4];
    Vista vista = new Vista();
    
    private void llenarLista(){
        this.docentesHC[0] = new HoraCatedra("Especialista en Sistemas de Información", 5, 6000000, 5000000, "Ingeniería", "Ingeniera de Sistemas", "Facultad de Ingeniería", 43668133, "Sonia", "Cardona", "20-01-1950", "Copacabana", "M", 6,"I");
        this.docentesHC[1] = new HoraCatedra("Especialista en Sistemas Embebidos", 4, 5000000, 3000000, "Ingeniería", "Ingeniera de Procesos", "Facultad de Ingeniería", 23581345, "Camila", "Botero", "05-12-1990", "Copacabana", "M", 6,"I");
        this.docentesHC[2] = new HoraCatedra("Magíster en Ingeniería", 6, 4000000, 4000000, "Ingeniería", "Ingeniero de Sistemas", "Facultad de Ingeniería", 32345465, "Pablo", "Vélez", "23-11-1970", "Medellín", "H", 5,"I");
        this.docentesHC[3] = new HoraCatedra("MBA", 7, 3000000, 7000000, "Administración", "Administrador de Negocios", "Facultad de Administración", 43668133, "Fabio", "López", "20-03-1980", "México", "H", 6,"I");
    }
    
    public HoraCatedra[] listaDocentesHC() {
        llenarLista();
        return this.docentesHC;
    }

    @Override
    public String imprimirEncabezadoLista() {
        return "ID      " + "\t" + "Nombre" + "\t   " + "Apellido     " + "Fecha de nacimiento" + "   "  + "Género" + "\t" + "   "  + "Área de formación" + "\t" + "      " + "Título profesional" + "\t" + "    " + "Unidad académica";
    }
    
    @Override
    public String mostrarDatos() {
        for (int i = 0; i < listaDocentesHC().length; i++) {
            System.out.println(docentesHC[i].mostrarDatosDocente());
        }
        return "";
    }
    
    @Override
    public String mostrarNombres(){
        for (int i = 0; i < listaDocentesHC().length; i++) {
            System.out.println(i+1 + ". " + docentesHC[i].getNomPersona());
        }
        return "";
    }
   

    @Override
    public double mostrarSalarioNeto() {
        double salarioNeto =0;
        switch(vista.getOpcionHC()){
            case 1:
                salarioNeto = docentesHC[0].calcularSueldo();
                break;
            case 2:
                salarioNeto = docentesHC[1].calcularSueldo();
                break;
            case 3:
                salarioNeto = docentesHC[2].calcularSueldo();
                break;
            case 4:
                salarioNeto = docentesHC[3].calcularSueldo();
                break;
        }
        return salarioNeto;
    }
    
    @Override
    public double mostrarSalarioBase() {
        double salarioBase =0;
        switch(vista.getOpcionHC()){
            case 1:
                salarioBase = docentesHC[0].getSalario();
                break;
            case 2:
                salarioBase = docentesHC[1].getSalario();
                break;
            case 3:
                salarioBase = docentesHC[2].getSalario();
                break;
            case 4:
                salarioBase = docentesHC[3].getSalario();
                break;
        }
        return salarioBase;
    }
    
    @Override
    public double mostrarEPS() {
        double montoEPS =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoEPS = docentesHC[0].calculaEPS(docentesHC[0].getSalario());
                break;
            case 2:
                montoEPS = docentesHC[1].calculaEPS(docentesHC[1].getSalario());
                break;
            case 3:
                montoEPS = docentesHC[2].calculaEPS(docentesHC[2].getSalario());
                break;
            case 4:
                montoEPS = docentesHC[3].calculaEPS(docentesHC[3].getSalario());
                break;
        }
        return montoEPS;
    }

    @Override
    public double mostrarPension() {
        double montoPension =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoPension = docentesHC[0].calculaPension(docentesHC[0].getSalario());
                break;
            case 2:
                montoPension = docentesHC[1].calculaPension(docentesHC[1].getSalario());
                break;
            case 3:
                montoPension = docentesHC[2].calculaPension(docentesHC[2].getSalario());
                break;
            case 4:
                montoPension = docentesHC[3].calculaPension(docentesHC[3].getSalario());
                break;
        }
        return montoPension;
    }

    @Override
    public double mostrarARL() {
        double montoARL =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoARL = docentesHC[0].calculaARL(docentesHC[0].getSalario());
                break;
            case 2:
                montoARL = docentesHC[1].calculaARL(docentesHC[1].getSalario());
                break;
            case 3:
                montoARL = docentesHC[2].calculaARL(docentesHC[2].getSalario());
                break;
            case 4:
                montoARL = docentesHC[3].calculaARL(docentesHC[3].getSalario());
                break;
        }
        return montoARL;
    }

    @Override
    public double mostrarSena() {
        double montoSENA =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoSENA = docentesHC[0].calculaSENA(docentesHC[0].getSalario());
                break;
            case 2:
                montoSENA = docentesHC[1].calculaSENA(docentesHC[1].getSalario());
                break;
            case 3:
                montoSENA = docentesHC[2].calculaSENA(docentesHC[2].getSalario());
                break;
            case 4:
                montoSENA = docentesHC[3].calculaSENA(docentesHC[3].getSalario());
                break;
        }
        return montoSENA;
    }

    @Override
    public double mostrarCajas() {
        double montoCajas =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoCajas = docentesHC[0].calculaCajas(docentesHC[0].getSalario());
                break;
            case 2:
                montoCajas = docentesHC[1].calculaCajas(docentesHC[1].getSalario());
                break;
            case 3:
                montoCajas = docentesHC[2].calculaCajas(docentesHC[2].getSalario());
                break;
            case 4:
                montoCajas = docentesHC[3].calculaCajas(docentesHC[3].getSalario());
                break;
        }
        return montoCajas;
    }

    @Override
    public double mostrarICBF() {
        double montoICBF =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoICBF = docentesHC[0].calculaICBF(docentesHC[0].getSalario());
                break;
            case 2:
                montoICBF = docentesHC[1].calculaICBF(docentesHC[1].getSalario());
                break;
            case 3:
                montoICBF = docentesHC[2].calculaICBF(docentesHC[2].getSalario());
                break;
            case 4:
                montoICBF = docentesHC[3].calculaICBF(docentesHC[3].getSalario());
                break;
        }
        return montoICBF;
    }

    @Override
    public double mostrarAuxilio() {
        double montoAuxilio =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoAuxilio = docentesHC[0].calculaAuxilio(docentesHC[0].getSalario());
                break;
            case 2:
                montoAuxilio = docentesHC[1].calculaAuxilio(docentesHC[1].getSalario());
                break;
            case 3:
                montoAuxilio = docentesHC[2].calculaAuxilio(docentesHC[2].getSalario());
                break;
            case 4:
                montoAuxilio = docentesHC[3].calculaAuxilio(docentesHC[3].getSalario());
                break;
        }
        return montoAuxilio;
    }

    @Override
    public double mostrarLiquidacion() {
        double montoLiquidacion =0;
        switch(vista.getOpcionHC()){
            case 1:
                montoLiquidacion = docentesHC[0].liquidarHC();
                break;
            case 2:
                montoLiquidacion = docentesHC[1].liquidarHC();
                break;
            case 3:
                montoLiquidacion = docentesHC[2].liquidarHC();
                break;
            case 4:
                montoLiquidacion = docentesHC[3].liquidarHC();
                break;
        }
        return montoLiquidacion;
    }

}
