/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controllerImpl.Datos;
import modelo.tipoDocente.TiempoCompleto;
import vista.Vista;

/**
 *
 * @author ANA_MARIA
 */
public class DocenteTiempoCompleto implements Datos {
    TiempoCompleto docentesTC[] = new TiempoCompleto[4];
    Vista vista = new Vista();
    
    private void llenarLista(){
        this.docentesTC[0] = new TiempoCompleto("Categoría 7", 7, 650000, 3, "Ingeniería", "Ingeniera de Calidad", "Facultad de Ingeniería", 43031609, "Ofelia", "Suárez", "01-01-1950", "Medellín", "M", 3,"I");
        this.docentesTC[1] = new TiempoCompleto("Categoría 6", 5, 1000000, 5, "Ingeniería", "Ingeniero de Sistemas", "Facultad de Ingeniería", 435678435, "Elkin", "Ospina", "01-12-1990", "Copacabana", "H", 4,"I");
        this.docentesTC[2] = new TiempoCompleto("Categoría 5", 4, 1500000, 2, "Ingeniería", "Ingeniero de Sistemas", "Facultad de Ingeniería", 97856784, "José", "Carmona", "12-03-1980", "Bello", "H", 2,"I");
        this.docentesTC[3] = new TiempoCompleto("Categoría 4", 7, 850000, 8, "Ingeniería", "Ingeniero de Diseño", "Facultad de Ingeniería", 78653467, "Óscar", "Castro", "13-01-1940", "Medellín", "H", 5,"I");
    }
    
    public TiempoCompleto[] listaDocentesTC() {
        llenarLista();
        return this.docentesTC;
    }

    @Override
    public String imprimirEncabezadoLista() {
        return "ID      " + "\t" + "Nombre" + "\t   " + "Apellido     " + "Fecha de nacimiento" + "   "  + "Género" + "\t" + "   "  + "Área de formación" + "\t" + "      " + "Título profesional" + "\t" + "    " + "Unidad académica";
    }

    @Override
    public String mostrarDatos() {
        for (int i = 0; i < listaDocentesTC().length; i++) {
            System.out.println(docentesTC[i].mostrarDatosDocente());
        }
        return "";
    }
    
    @Override
    public String mostrarNombres(){
        for (int i = 0; i < listaDocentesTC().length; i++) {
            System.out.println(i+1 + ". " + docentesTC[i].getNomPersona());
        }
        return "";
    }
    
    @Override
    public double mostrarSalarioNeto() {
        double salarioNeto =0;
        switch(vista.getOpcionDC()){
            case 1:
                salarioNeto = docentesTC[0].calcularSueldo();
                break;
            case 2:
                salarioNeto = docentesTC[1].calcularSueldo();
                break;
            case 3:
                salarioNeto = docentesTC[2].calcularSueldo();
                break;
            case 4:
                salarioNeto = docentesTC[3].calcularSueldo();
                break;
        }
        return salarioNeto;
    }
    
    @Override
    public double mostrarSalarioBase() {
        double salarioBase =0;
        switch(vista.getOpcionDC()){
            case 1:
                salarioBase = docentesTC[0].getSalario();
                break;
            case 2:
                salarioBase = docentesTC[1].getSalario();
                break;
            case 3:
                salarioBase = docentesTC[2].getSalario();
                break;
            case 4:
                salarioBase = docentesTC[3].getSalario();
                break;
        }
        return salarioBase;
    }
    
    @Override
    public double mostrarEPS() {
        double montoEPS =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoEPS = docentesTC[0].calculaEPS(docentesTC[0].getSalario());
                break;
            case 2:
                montoEPS = docentesTC[1].calculaEPS(docentesTC[1].getSalario());
                break;
            case 3:
                montoEPS = docentesTC[2].calculaEPS(docentesTC[2].getSalario());
                break;
            case 4:
                montoEPS = docentesTC[3].calculaEPS(docentesTC[3].getSalario());
                break;
        }
        return montoEPS;
    }

    @Override
    public double mostrarPension() {
        double montoPension =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoPension = docentesTC[0].calculaPension(docentesTC[0].getSalario());
                break;
            case 2:
                montoPension = docentesTC[1].calculaPension(docentesTC[1].getSalario());
                break;
            case 3:
                montoPension = docentesTC[2].calculaPension(docentesTC[2].getSalario());
                break;
            case 4:
                montoPension = docentesTC[3].calculaPension(docentesTC[3].getSalario());
                break;
        }
        return montoPension;
    }

    @Override
    public double mostrarARL() {
        double montoARL =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoARL = docentesTC[0].calculaARL(docentesTC[0].getSalario());
                break;
            case 2:
                montoARL = docentesTC[1].calculaARL(docentesTC[1].getSalario());
                break;
            case 3:
                montoARL = docentesTC[2].calculaARL(docentesTC[2].getSalario());
                break;
            case 4:
                montoARL = docentesTC[3].calculaARL(docentesTC[3].getSalario());
                break;
        }
        return montoARL;
    }

    @Override
    public double mostrarSena() {
        double montoSENA =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoSENA = docentesTC[0].calculaSENA(docentesTC[0].getSalario());
                break;
            case 2:
                montoSENA = docentesTC[1].calculaSENA(docentesTC[1].getSalario());
                break;
            case 3:
                montoSENA = docentesTC[2].calculaSENA(docentesTC[2].getSalario());
                break;
            case 4:
                montoSENA = docentesTC[3].calculaSENA(docentesTC[3].getSalario());
                break;
        }
        return montoSENA;
    }

    @Override
    public double mostrarCajas() {
        double montoCajas =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoCajas = docentesTC[0].calculaCajas(docentesTC[0].getSalario());
                break;
            case 2:
                montoCajas = docentesTC[1].calculaCajas(docentesTC[1].getSalario());
                break;
            case 3:
                montoCajas = docentesTC[2].calculaCajas(docentesTC[2].getSalario());
                break;
            case 4:
                montoCajas = docentesTC[3].calculaCajas(docentesTC[3].getSalario());
                break;
        }
        return montoCajas;
    }

    @Override
    public double mostrarICBF() {
        double montoICBF =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoICBF = docentesTC[0].calculaICBF(docentesTC[0].getSalario());
                break;
            case 2:
                montoICBF = docentesTC[1].calculaICBF(docentesTC[1].getSalario());
                break;
            case 3:
                montoICBF = docentesTC[2].calculaICBF(docentesTC[2].getSalario());
                break;
            case 4:
                montoICBF = docentesTC[3].calculaICBF(docentesTC[3].getSalario());
                break;
        }
        return montoICBF;
    }

    @Override
    public double mostrarAuxilio() {
        double montoAuxilio =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoAuxilio = docentesTC[0].calculaAuxilio(docentesTC[0].getSalario());
                break;
            case 2:
                montoAuxilio = docentesTC[1].calculaAuxilio(docentesTC[1].getSalario());
                break;
            case 3:
                montoAuxilio = docentesTC[2].calculaAuxilio(docentesTC[2].getSalario());
                break;
            case 4:
                montoAuxilio = docentesTC[3].calculaAuxilio(docentesTC[3].getSalario());
                break;
        }
        return montoAuxilio;
    }

    @Override
    public double mostrarLiquidacion() {
        double montoLiquidacion =0;
        switch(vista.getOpcionDC()){
            case 1:
                montoLiquidacion = docentesTC[0].liquidarTC();
                break;
            case 2:
                montoLiquidacion = docentesTC[1].liquidarTC();
                break;
            case 3:
                montoLiquidacion = docentesTC[2].liquidarTC();
                break;
            case 4:
                montoLiquidacion = docentesTC[3].liquidarTC();
                break;
        }
        return montoLiquidacion;
    }

}
