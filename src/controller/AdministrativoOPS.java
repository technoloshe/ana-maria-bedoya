/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controllerImpl.Datos;
import modelo.administrativo.OPS;
import vista.Vista;

/**
 *
 * @author ANA_MARIA
 */
public class AdministrativoOPS implements Datos{
    OPS adminOPS[] = new OPS[4];
    Vista vista = new Vista();
    
    private void llenarLista() {
        this.adminOPS[0] = new OPS("20-03-2019", 4, 850000, 800000, "Innovación", "Ingeniero de Diseño de Producto", 11456783, "Luis", "Sandoval", "10-03-1989", "Medellín", "H", 3,"IV");
        this.adminOPS[1] = new OPS("14-05-2019", 2, 1000000, 1000000, "Marketing ", "Comunicador Social", 45694866, "Danilo", "Salas", "07-03-1967", "Medellín", "H", 4,"IV");
        this.adminOPS[2] = new OPS("01-02-2019", 5, 1500000, 1500000, "Negocios   ", "Negociador Internacional", 56469854, "Jorge", "Carmona", "12-03-1997", "Medellín", "H", 5,"IV");
        this.adminOPS[3] = new OPS("20-05-2019", 1, 1200000, 1200000, "Ingeniería", "Ingeniero Mecánico", 56560546, "Héctor", "Ochoa", "14-09-1989", "Medellín", "H", 5,"IV");
    }
    
    public OPS[] listaAdminOPS() {
        llenarLista();
        return this.adminOPS;
    }
    
    @Override
    public String imprimirEncabezadoLista() {
        return "ID      " + "\t" + "Nombre" + "\t   " + "Apellido     " + "Fecha de nacimiento" + "   "  + "Género" + "\t" + "     "  + "Dependencia" + "\t" + "     " + "Título";
    }

    @Override
    public String mostrarDatos() {
        for (int i = 0; i < listaAdminOPS().length; i++) {
            System.out.println(adminOPS[i].mostrarDatosAdmin());
        }
        return "";
    }

    @Override
    public String mostrarNombres() {
        for (int i = 0; i < listaAdminOPS().length; i++) {
            System.out.println(i + 1 + ". " + adminOPS[i].getNomPersona());
        }
        return "";
    }

    @Override
    public double mostrarSalarioNeto() {
        double salarioNeto = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                salarioNeto = adminOPS[0].calcularSueldo();
                break;
            case 2:
                salarioNeto = adminOPS[1].calcularSueldo();
                break;
            case 3:
                salarioNeto = adminOPS[2].calcularSueldo();
                break;
            case 4:
                salarioNeto = adminOPS[3].calcularSueldo();
                break;
        }
        return salarioNeto;
    }

    @Override
    public double mostrarSalarioBase() {
        double salarioBase = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                salarioBase = adminOPS[0].getSalario();
                break;
            case 2:
                salarioBase = adminOPS[1].getSalario();
                break;
            case 3:
                salarioBase = adminOPS[2].getSalario();
                break;
            case 4:
                salarioBase = adminOPS[3].getSalario();
                break;
        }
        return salarioBase;
    }

    @Override
    public double mostrarEPS() {
        double montoEPS = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoEPS = adminOPS[0].calculaEPS(adminOPS[0].getSalario());
                break;
            case 2:
                montoEPS = adminOPS[1].calculaEPS(adminOPS[1].getSalario());
                break;
            case 3:
                montoEPS = adminOPS[2].calculaEPS(adminOPS[2].getSalario());
                break;
            case 4:
                montoEPS = adminOPS[3].calculaEPS(adminOPS[3].getSalario());
                break;
        }
        return montoEPS;
    }

    @Override
    public double mostrarPension() {
        double montoPension = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoPension = adminOPS[0].calculaPension(adminOPS[0].getSalario());
                break;
            case 2:
                montoPension = adminOPS[1].calculaPension(adminOPS[1].getSalario());
                break;
            case 3:
                montoPension = adminOPS[2].calculaPension(adminOPS[2].getSalario());
                break;
            case 4:
                montoPension = adminOPS[3].calculaPension(adminOPS[3].getSalario());
                break;
        }
        return montoPension;
    }

    @Override
    public double mostrarARL() {
        double montoARL = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoARL = adminOPS[0].calculaARL(adminOPS[0].getSalario());
                break;
            case 2:
                montoARL = adminOPS[1].calculaARL(adminOPS[1].getSalario());
                break;
            case 3:
                montoARL = adminOPS[2].calculaARL(adminOPS[2].getSalario());
                break;
            case 4:
                montoARL = adminOPS[3].calculaARL(adminOPS[3].getSalario());
                break;
        }
        return montoARL;
    }

    @Override
    public double mostrarSena() {
        double montoSENA = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoSENA = adminOPS[0].calculaSENA(adminOPS[0].getSalario());
                break;
            case 2:
                montoSENA = adminOPS[1].calculaSENA(adminOPS[1].getSalario());
                break;
            case 3:
                montoSENA = adminOPS[2].calculaSENA(adminOPS[2].getSalario());
                break;
            case 4:
                montoSENA = adminOPS[3].calculaSENA(adminOPS[3].getSalario());
                break;
        }
        return montoSENA;
    }

    @Override
    public double mostrarCajas() {
        double montoCajas = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoCajas = adminOPS[0].calculaCajas(adminOPS[0].getSalario());
                break;
            case 2:
                montoCajas = adminOPS[1].calculaCajas(adminOPS[1].getSalario());
                break;
            case 3:
                montoCajas = adminOPS[2].calculaCajas(adminOPS[2].getSalario());
                break;
            case 4:
                montoCajas = adminOPS[3].calculaCajas(adminOPS[3].getSalario());
                break;
        }
        return montoCajas;
    }

    @Override
    public double mostrarICBF() {
        double montoICBF = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoICBF = adminOPS[0].calculaICBF(adminOPS[0].getSalario());
                break;
            case 2:
                montoICBF = adminOPS[1].calculaICBF(adminOPS[1].getSalario());
                break;
            case 3:
                montoICBF = adminOPS[2].calculaICBF(adminOPS[2].getSalario());
                break;
            case 4:
                montoICBF = adminOPS[3].calculaICBF(adminOPS[3].getSalario());
                break;
        }
        return montoICBF;
    }

    @Override
    public double mostrarAuxilio() {
        double montoAuxilio = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoAuxilio = adminOPS[0].calculaAuxilio(adminOPS[0].getSalario());
                break;
            case 2:
                montoAuxilio = adminOPS[1].calculaAuxilio(adminOPS[1].getSalario());
                break;
            case 3:
                montoAuxilio = adminOPS[2].calculaAuxilio(adminOPS[2].getSalario());
                break;
            case 4:
                montoAuxilio = adminOPS[3].calculaAuxilio(adminOPS[3].getSalario());
                break;
        }
        return montoAuxilio;
    }

    @Override
    public double mostrarLiquidacion() {
        double montoLiquidacion = 0;
        switch (vista.getOpcionOPS()) {
            case 1:
                montoLiquidacion = adminOPS[0].liquidarValorContrato();
                break;
            case 2:
                montoLiquidacion = adminOPS[1].liquidarValorContrato();
                break;
            case 3:
                montoLiquidacion = adminOPS[2].liquidarValorContrato();
                break;
            case 4:
                montoLiquidacion = adminOPS[3].liquidarValorContrato();
                break;
        }
        return montoLiquidacion;
    }

}
