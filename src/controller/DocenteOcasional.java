/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controllerImpl.Datos;
import modelo.tipoDocente.Ocasional;
import vista.Vista;

/**
 *
 * @author ANA_MARIA
 */
public class DocenteOcasional implements Datos {
    Ocasional docentesOC[] = new Ocasional[4];
    Vista vista = new Vista();
    
    private void llenarLista(){
        this.docentesOC[0] = new Ocasional("Magíster en Ingeniería", 6, 2000000, "Ingeniería", "Ingeniero de Desarrollo", "Facultad de Ingeniería", 1152464040, "Ana", "Bedoya", "27-09-1997", "Bello", "M", 6,"I");
        this.docentesOC[1] = new Ocasional("Especialista en Finanzas", 3, 5000000, "Finanzas  ", "Profesional en Finanzas", "Facultad de Finanzas", 75645738, "Luisa", "Tamayo", "27-03-1997", "Medellín", "M", 5,"I");
        this.docentesOC[2] = new Ocasional("Magíster en Proyectos", 2, 1000000, "Ingeniería", "Ingeniero de Sistemas", "Facultad de Ingeniería", 23457497, "Pedro", "Tobón", "20-02-1954", "Bello", "H", 5,"I");
        this.docentesOC[3] = new Ocasional("Especialista en Gerencia", 5, 6000000, "Ingeniería", "Ingeniero de Sistemas", "Facultad de Ingeniería", 45738493, "Jhon", "Vásquez", "12-03-1987", "USA", "H", 6,"I");
    }

    public Ocasional[] listaDocentesOC() {
        llenarLista();
        return this.docentesOC;
    }
    
    @Override
    public String imprimirEncabezadoLista() {
        return "ID      " + "\t" + "Nombre" + "\t   " + "Apellido     " + "Fecha de nacimiento" + "   "  + "Género" + "\t" + "   "  + "Área de formación" + "\t" + "      " + "Título profesional" + "\t" + "    " + "Unidad académica";
    }
    
    @Override
    public String mostrarDatos() {
        for (int i = 0; i < listaDocentesOC().length; i++) {
            System.out.println(docentesOC[i].mostrarDatosDocente());
        }
        return "";
    }
    
    @Override
    public String mostrarNombres(){
        for (int i = 0; i < listaDocentesOC().length; i++) {
            System.out.println(i+1 + ". " + docentesOC[i].getNomPersona());
        }
        return "";
    }
   

    @Override
    public double mostrarSalarioNeto() {
        double salarioNeto =0;
        switch(vista.getOpcionOC()){
            case 1:
                salarioNeto = docentesOC[0].calcularSueldo();
                break;
            case 2:
                salarioNeto = docentesOC[1].calcularSueldo();
                break;
            case 3:
                salarioNeto = docentesOC[2].calcularSueldo();
                break;
            case 4:
                salarioNeto = docentesOC[3].calcularSueldo();
                break;
        }
        return salarioNeto;
    }
    
    @Override
    public double mostrarSalarioBase() {
        double salarioBase =0;
        switch(vista.getOpcionOC()){
            case 1:
                salarioBase = docentesOC[0].getSalario();
                break;
            case 2:
                salarioBase = docentesOC[1].getSalario();
                break;
            case 3:
                salarioBase = docentesOC[2].getSalario();
                break;
            case 4:
                salarioBase = docentesOC[3].getSalario();
                break;
        }
        return salarioBase;
    }
    
    @Override
    public double mostrarEPS() {
        double montoEPS =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoEPS = docentesOC[0].calculaEPS(docentesOC[0].getSalario());
                break;
            case 2:
                montoEPS = docentesOC[1].calculaEPS(docentesOC[1].getSalario());
                break;
            case 3:
                montoEPS = docentesOC[2].calculaEPS(docentesOC[2].getSalario());
                break;
            case 4:
                montoEPS = docentesOC[3].calculaEPS(docentesOC[3].getSalario());
                break;
        }
        return montoEPS;
    }

    @Override
    public double mostrarPension() {
        double montoPension =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoPension = docentesOC[0].calculaPension(docentesOC[0].getSalario());
                break;
            case 2:
                montoPension = docentesOC[1].calculaPension(docentesOC[1].getSalario());
                break;
            case 3:
                montoPension = docentesOC[2].calculaPension(docentesOC[2].getSalario());
                break;
            case 4:
                montoPension = docentesOC[3].calculaPension(docentesOC[3].getSalario());
                break;
        }
        return montoPension;
    }

    @Override
    public double mostrarARL() {
        double montoARL =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoARL = docentesOC[0].calculaARL(docentesOC[0].getSalario());
                break;
            case 2:
                montoARL = docentesOC[1].calculaARL(docentesOC[1].getSalario());
                break;
            case 3:
                montoARL = docentesOC[2].calculaARL(docentesOC[2].getSalario());
                break;
            case 4:
                montoARL = docentesOC[3].calculaARL(docentesOC[3].getSalario());
                break;
        }
        return montoARL;
    }

    @Override
    public double mostrarSena() {
        double montoSENA =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoSENA = docentesOC[0].calculaSENA(docentesOC[0].getSalario());
                break;
            case 2:
                montoSENA = docentesOC[1].calculaSENA(docentesOC[1].getSalario());
                break;
            case 3:
                montoSENA = docentesOC[2].calculaSENA(docentesOC[2].getSalario());
                break;
            case 4:
                montoSENA = docentesOC[3].calculaSENA(docentesOC[3].getSalario());
                break;
        }
        return montoSENA;
    }

    @Override
    public double mostrarCajas() {
        double montoCajas =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoCajas = docentesOC[0].calculaCajas(docentesOC[0].getSalario());
                break;
            case 2:
                montoCajas = docentesOC[1].calculaCajas(docentesOC[1].getSalario());
                break;
            case 3:
                montoCajas = docentesOC[2].calculaCajas(docentesOC[2].getSalario());
                break;
            case 4:
                montoCajas = docentesOC[3].calculaCajas(docentesOC[3].getSalario());
                break;
        }
        return montoCajas;
    }

    @Override
    public double mostrarICBF() {
        double montoICBF =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoICBF = docentesOC[0].calculaICBF(docentesOC[0].getSalario());
                break;
            case 2:
                montoICBF = docentesOC[1].calculaICBF(docentesOC[1].getSalario());
                break;
            case 3:
                montoICBF = docentesOC[2].calculaICBF(docentesOC[2].getSalario());
                break;
            case 4:
                montoICBF = docentesOC[3].calculaICBF(docentesOC[3].getSalario());
                break;
        }
        return montoICBF;
    }

    @Override
    public double mostrarAuxilio() {
        double montoAuxilio =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoAuxilio = docentesOC[0].calculaAuxilio(docentesOC[0].getSalario());
                break;
            case 2:
                montoAuxilio = docentesOC[1].calculaAuxilio(docentesOC[1].getSalario());
                break;
            case 3:
                montoAuxilio = docentesOC[2].calculaAuxilio(docentesOC[2].getSalario());
                break;
            case 4:
                montoAuxilio = docentesOC[3].calculaAuxilio(docentesOC[3].getSalario());
                break;
        }
        return montoAuxilio;
    }

    @Override
    public double mostrarLiquidacion() {
        double montoLiquidacion =0;
        switch(vista.getOpcionOC()){
            case 1:
                montoLiquidacion = docentesOC[0].liquidarOC();
                break;
            case 2:
                montoLiquidacion = docentesOC[1].liquidarOC();
                break;
            case 3:
                montoLiquidacion = docentesOC[2].liquidarOC();
                break;
            case 4:
                montoLiquidacion = docentesOC[3].liquidarOC();
                break;
        }
        return montoLiquidacion;
    }

}
