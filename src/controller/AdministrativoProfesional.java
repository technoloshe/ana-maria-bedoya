/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controllerImpl.Datos;
import modelo.administrativo.planta.Profesional;
import vista.Vista;

/**
 *
 * @author ANA_MARIA
 */
public class AdministrativoProfesional implements Datos {
    Profesional adminPro[] = new Profesional[4];
    Vista vista = new Vista();

    private void llenarLista() {
        this.adminPro[0] = new Profesional(3, 1800000, "01-09-2018", 10, "Finanzas", "Profesional en Finanzas", 99435675, "Alex", "Palacios", "10-04-1990", "Medellín", "H", 5, "I");
        this.adminPro[1] = new Profesional(4, 2000000, "01-02-2019", 5, "Tesorería", "Contador Público", 46348873, "Alonso", "Giraldo", "10-09-1997", "Medellín", "H", 3, "I");
        this.adminPro[2] = new Profesional(4, 2500000, "12-05-2019", 2, "Tesorería", "Profesional en Finanzas", 113423445, "Daniela", "Loaiza", "27-05-1980", "Medellín", "M", 6, "I");
        this.adminPro[3] = new Profesional(2, 1500000, "06-06-2019", 1, "Marketing", "Comunicadora Social", 56987657, "Andrea", "Giraldo", "10-03-1997", "Medellín", "M", 6, "I");
    }

    public Profesional[] listaAdminPro() {
        llenarLista();
        return this.adminPro;
    }

    @Override
    public String imprimirEncabezadoLista() {
        return "ID      " + "\t" + "Nombre" + "\t   " + "Apellido     " + "Fecha de nacimiento" + "   "  + "Género" + "\t" + "     "  + "Dependencia" + "\t" + "     " + "Título";
    }

    @Override
    public String mostrarDatos() {
        for (int i = 0; i < listaAdminPro().length; i++) {
            System.out.println(adminPro[i].mostrarDatosAdmin());
        }
        return "";
    }

    @Override
    public String mostrarNombres() {
        for (int i = 0; i < listaAdminPro().length; i++) {
            System.out.println(i + 1 + ". " + adminPro[i].getNomPersona());
        }
        return "";
    }

    @Override
    public double mostrarSalarioNeto() {
        double salarioNeto = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                salarioNeto = adminPro[0].calcularSueldo();
                break;
            case 2:
                salarioNeto = adminPro[1].calcularSueldo();
                break;
            case 3:
                salarioNeto = adminPro[2].calcularSueldo();
                break;
            case 4:
                salarioNeto = adminPro[3].calcularSueldo();
                break;
        }
        return salarioNeto;
    }

    @Override
    public double mostrarSalarioBase() {
        double salarioBase = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                salarioBase = adminPro[0].getSalario();
                break;
            case 2:
                salarioBase = adminPro[1].getSalario();
                break;
            case 3:
                salarioBase = adminPro[2].getSalario();
                break;
            case 4:
                salarioBase = adminPro[3].getSalario();
                break;
        }
        return salarioBase;
    }

    @Override
    public double mostrarEPS() {
        double montoEPS = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoEPS = adminPro[0].calculaEPS(adminPro[0].getSalario());
                break;
            case 2:
                montoEPS = adminPro[1].calculaEPS(adminPro[1].getSalario());
                break;
            case 3:
                montoEPS = adminPro[2].calculaEPS(adminPro[2].getSalario());
                break;
            case 4:
                montoEPS = adminPro[3].calculaEPS(adminPro[3].getSalario());
                break;
        }
        return montoEPS;
    }

    @Override
    public double mostrarPension() {
        double montoPension = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoPension = adminPro[0].calculaPension(adminPro[0].getSalario());
                break;
            case 2:
                montoPension = adminPro[1].calculaPension(adminPro[1].getSalario());
                break;
            case 3:
                montoPension = adminPro[2].calculaPension(adminPro[2].getSalario());
                break;
            case 4:
                montoPension = adminPro[3].calculaPension(adminPro[3].getSalario());
                break;
        }
        return montoPension;
    }

    @Override
    public double mostrarARL() {
        double montoARL = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoARL = adminPro[0].calculaARL(adminPro[0].getSalario());
                break;
            case 2:
                montoARL = adminPro[1].calculaARL(adminPro[1].getSalario());
                break;
            case 3:
                montoARL = adminPro[2].calculaARL(adminPro[2].getSalario());
                break;
            case 4:
                montoARL = adminPro[3].calculaARL(adminPro[3].getSalario());
                break;
        }
        return montoARL;
    }

    @Override
    public double mostrarSena() {
        double montoSENA = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoSENA = adminPro[0].calculaSENA(adminPro[0].getSalario());
                break;
            case 2:
                montoSENA = adminPro[1].calculaSENA(adminPro[1].getSalario());
                break;
            case 3:
                montoSENA = adminPro[2].calculaSENA(adminPro[2].getSalario());
                break;
            case 4:
                montoSENA = adminPro[3].calculaSENA(adminPro[3].getSalario());
                break;
        }
        return montoSENA;
    }

    @Override
    public double mostrarCajas() {
        double montoCajas = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoCajas = adminPro[0].calculaCajas(adminPro[0].getSalario());
                break;
            case 2:
                montoCajas = adminPro[1].calculaCajas(adminPro[1].getSalario());
                break;
            case 3:
                montoCajas = adminPro[2].calculaCajas(adminPro[2].getSalario());
                break;
            case 4:
                montoCajas = adminPro[3].calculaCajas(adminPro[3].getSalario());
                break;
        }
        return montoCajas;
    }

    @Override
    public double mostrarICBF() {
        double montoICBF = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoICBF = adminPro[0].calculaICBF(adminPro[0].getSalario());
                break;
            case 2:
                montoICBF = adminPro[1].calculaICBF(adminPro[1].getSalario());
                break;
            case 3:
                montoICBF = adminPro[2].calculaICBF(adminPro[2].getSalario());
                break;
            case 4:
                montoICBF = adminPro[3].calculaICBF(adminPro[3].getSalario());
                break;
        }
        return montoICBF;
    }

    @Override
    public double mostrarAuxilio() {
        double montoAuxilio = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoAuxilio = adminPro[0].calculaAuxilio(adminPro[0].getSalario());
                break;
            case 2:
                montoAuxilio = adminPro[1].calculaAuxilio(adminPro[1].getSalario());
                break;
            case 3:
                montoAuxilio = adminPro[2].calculaAuxilio(adminPro[2].getSalario());
                break;
            case 4:
                montoAuxilio = adminPro[3].calculaAuxilio(adminPro[3].getSalario());
                break;
        }
        return montoAuxilio;
    }

    @Override
    public double mostrarLiquidacion() {
        double montoLiquidacion = 0;
        switch (vista.getOpcionPROF()) {
            case 1:
                montoLiquidacion = adminPro[0].liquidarProf();
                break;
            case 2:
                montoLiquidacion = adminPro[1].liquidarProf();
                break;
            case 3:
                montoLiquidacion = adminPro[2].liquidarProf();
                break;
            case 4:
                montoLiquidacion = adminPro[3].liquidarProf();
                break;
        }
        return montoLiquidacion;
    }

}
