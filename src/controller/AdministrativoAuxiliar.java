/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controllerImpl.Datos;
import modelo.administrativo.planta.Auxiliar;
import vista.Vista;

/**
 *
 * @author ANA_MARIA
 */
public class AdministrativoAuxiliar implements Datos{
    Auxiliar adminAux[] = new Auxiliar[4];
    Vista vista = new Vista();
    
    private void llenarLista(){
        this.adminAux[0] = new Auxiliar(1, 1800000, "01-05-2019", 2, "Auditoría ", "Auditor interno", 113456435, "Felipe", "Gutiérrez", "02-04-1956", "Medellín", "H", 6,"II");
        this.adminAux[1] = new Auxiliar(1, 1500000, "01-01-2019", 7, "Tecnología", "Asistente administrativo", 457389458, "Sandra", "Bedoya", "02-03-1968", "Medellín", "M", 3,"II");
        this.adminAux[2] = new Auxiliar(1, 1600000, "01-02-2019", 6, "Producción", "Auxiliar administrativo", 69056906, "Gustavo", "Castro", "14-06-1996", "Barbosa", "H", 4,"II");
        this.adminAux[3] = new Auxiliar(1, 1000000, "12-12-2018", 8, "Financiera", "Profesional en finanzas", 68605695, "Luis", "Lopera", "19-11-1978", "Girardota", "H", 5,"II");  
    }
    
    public Auxiliar[] listaAdminAux() {
        llenarLista();
        return this.adminAux;
    }

    @Override
    public String imprimirEncabezadoLista() {
        return "ID      " + "\t" + "Nombre" + "\t   " + "Apellido     " + "Fecha de nacimiento" + "   "  + "Género" + "\t" + "     "  + "Dependencia" + "\t" + "     " + "Título";
    }
    
    @Override
    public String mostrarDatos() {
        for (int i = 0; i < listaAdminAux().length; i++) {
            System.out.println(adminAux[i].mostrarDatosAdmin());
        }
        return "";
    }
    
    @Override
    public String mostrarNombres(){
        for (int i = 0; i < listaAdminAux().length; i++) {
            System.out.println(i+1 + ". " + adminAux[i].getNomPersona());
        }
        return "";
    }
   

    @Override
    public double mostrarSalarioNeto() {
        double salarioNeto =0;
        switch(vista.getOpcionAUX()){
            case 1:
                salarioNeto = adminAux[0].calcularSueldo();
                break;
            case 2:
                salarioNeto = adminAux[1].calcularSueldo();
                break;
            case 3:
                salarioNeto = adminAux[2].calcularSueldo();
                break;
            case 4:
                salarioNeto = adminAux[3].calcularSueldo();
                break;
        }
        return salarioNeto;
    }
    
    @Override
    public double mostrarSalarioBase() {
        double salarioBase =0;
        switch(vista.getOpcionAUX()){
            case 1:
                salarioBase = adminAux[0].getSalario();
                break;
            case 2:
                salarioBase = adminAux[1].getSalario();
                break;
            case 3:
                salarioBase = adminAux[2].getSalario();
                break;
            case 4:
                salarioBase = adminAux[3].getSalario();
                break;
        }
        return salarioBase;
    }
    
    @Override
    public double mostrarEPS() {
        double montoEPS =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoEPS = adminAux[0].calculaEPS(adminAux[0].getSalario());
                break;
            case 2:
                montoEPS = adminAux[1].calculaEPS(adminAux[1].getSalario());
                break;
            case 3:
                montoEPS = adminAux[2].calculaEPS(adminAux[2].getSalario());
                break;
            case 4:
                montoEPS = adminAux[3].calculaEPS(adminAux[3].getSalario());
                break;
        }
        return montoEPS;
    }

    @Override
    public double mostrarPension() {
        double montoPension =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoPension = adminAux[0].calculaPension(adminAux[0].getSalario());
                break;
            case 2:
                montoPension = adminAux[1].calculaPension(adminAux[1].getSalario());
                break;
            case 3:
                montoPension = adminAux[2].calculaPension(adminAux[2].getSalario());
                break;
            case 4:
                montoPension = adminAux[3].calculaPension(adminAux[3].getSalario());
                break;
        }
        return montoPension;
    }

    @Override
    public double mostrarARL() {
        double montoARL =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoARL = adminAux[0].calculaARL(adminAux[0].getSalario());
                break;
            case 2:
                montoARL = adminAux[1].calculaARL(adminAux[1].getSalario());
                break;
            case 3:
                montoARL = adminAux[2].calculaARL(adminAux[2].getSalario());
                break;
            case 4:
                montoARL = adminAux[3].calculaARL(adminAux[3].getSalario());
                break;
        }
        return montoARL;
    }

    @Override
    public double mostrarSena() {
        double montoSENA =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoSENA = adminAux[0].calculaSENA(adminAux[0].getSalario());
                break;
            case 2:
                montoSENA = adminAux[1].calculaSENA(adminAux[1].getSalario());
                break;
            case 3:
                montoSENA = adminAux[2].calculaSENA(adminAux[2].getSalario());
                break;
            case 4:
                montoSENA = adminAux[3].calculaSENA(adminAux[3].getSalario());
                break;
        }
        return montoSENA;
    }

    @Override
    public double mostrarCajas() {
        double montoCajas =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoCajas = adminAux[0].calculaCajas(adminAux[0].getSalario());
                break;
            case 2:
                montoCajas = adminAux[1].calculaCajas(adminAux[1].getSalario());
                break;
            case 3:
                montoCajas = adminAux[2].calculaCajas(adminAux[2].getSalario());
                break;
            case 4:
                montoCajas = adminAux[3].calculaCajas(adminAux[3].getSalario());
                break;
        }
        return montoCajas;
    }

    @Override
    public double mostrarICBF() {
        double montoICBF =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoICBF = adminAux[0].calculaICBF(adminAux[0].getSalario());
                break;
            case 2:
                montoICBF = adminAux[1].calculaICBF(adminAux[1].getSalario());
                break;
            case 3:
                montoICBF = adminAux[2].calculaICBF(adminAux[2].getSalario());
                break;
            case 4:
                montoICBF = adminAux[3].calculaICBF(adminAux[3].getSalario());
                break;
        }
        return montoICBF;
    }

    @Override
    public double mostrarAuxilio() {
        double montoAuxilio =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoAuxilio = adminAux[0].calculaAuxilio(adminAux[0].getSalario());
                break;
            case 2:
                montoAuxilio = adminAux[1].calculaAuxilio(adminAux[1].getSalario());
                break;
            case 3:
                montoAuxilio = adminAux[2].calculaAuxilio(adminAux[2].getSalario());
                break;
            case 4:
                montoAuxilio = adminAux[3].calculaAuxilio(adminAux[3].getSalario());
                break;
        }
        return montoAuxilio;
    }

    @Override
    public double mostrarLiquidacion() {
        double montoLiquidacion =0;
        switch(vista.getOpcionAUX()){
            case 1:
                montoLiquidacion = adminAux[0].liquidarAux();
                break;
            case 2:
                montoLiquidacion = adminAux[1].liquidarAux();
                break;
            case 3:
                montoLiquidacion = adminAux[2].liquidarAux();
                break;
            case 4:
                montoLiquidacion = adminAux[3].liquidarAux();
                break;
        }
        return montoLiquidacion;
    }

}
