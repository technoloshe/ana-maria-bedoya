**Proyecto Universidad:** Tiene como objetivo calcular el sueldo neto de trabajadores docentes y de administrativos. Este cálculo se efectúa teniendo en cuenta las deducciones que se hacen sobre el salario para la EPS, pensión, SENA y cajas de compensación. Así mismo, se tiene en cuenta el monto de salario que recibe el trabajador para saber si este es beneficiario del auxilio de transporte. 


**Instrucciones de ejecución:** Ejecutar la clase main del archivo Vista.java disponible en el paquete vista.

